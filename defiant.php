<?php

$pattern = "/\[(.*?)\]/";

$file = './example.txt';
$f = fopen($file, 'r');

if ($f) {
  $content = fread($f, filesize($file));
  fclose($f);
  preg_match_all($pattern, $content, $matches);
  
  foreach ($matches[1] as $match) {
    echo $match, PHP_EOL;
  }
}

?>
